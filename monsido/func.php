<?php

	// --
	// Monsido SYNC functions
	// --	
	$base_url = "https://app2.us.monsido.com/api/";
	
	function buildStructure($mysqli, $table, $unique_key,$fields)
	{
		print "---------------------------- \n";
		print "---------------------------- \n";
		print "Building INSERT QUERY STRING \n";
		print "---------------------------- \n";
		print "---------------------------- \n";
		
		$buid_create_table_mysql = "CREATE TABLE `".$table."` (\n";
		$buid_create_table_mysql .= "`id` int(11) unsigned NOT NULL AUTO_INCREMENT,\n";	
		$buid_create_table_mysql .= "`".$unique_key."` int(11) DEFAULT NULL,\n";
		if($table != "domains") {
			$existe_domain_id=false;
			foreach ($fields as $row_articuloxx => $value)
			{
				if($row_articuloxx != "domain_id")
				{
					$existe_domain_id=true;
				}
			}
			if($existe_domain_id == false)
			{
				$buid_create_table_mysql .= "`domain_id` int(11) DEFAULT NULL,\n";
			}
		}
				
		// ---
		// SOURCE STRUCTURE.
		foreach ($fields as $row_articuloxx => $value)
		{
			if($row_articuloxx != "id")
			{
				$type = "varchar(255)";
				$buid_create_table_mysql .= "`".$row_articuloxx."` ".$type." DEFAULT NULL,\n";
			}
		}
			
		$buid_create_table_mysql .= "PRIMARY KEY (`id`),\n";
		$buid_create_table_mysql .= "UNIQUE KEY `RECNO` (`".$unique_key."`)\n";
		$buid_create_table_mysql .= ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;\n";
		
		// Drop existing table
		print "DROP EXISTING TABLE \n";
		$sql = "DROP TABLE IF EXISTS ".$table."";
		if ($mysqli->query($sql)) { }
		if ($mysqli->errno) { printf("Could not insert record into table: %s<br />", $mysqli->error); }
							            
		print "CREATING NEW TABLE STRUCTURE \n";	
		$sql = "DROP TABLE ".$table;
		if ($mysqli->query($buid_create_table_mysql)) { }
		if ($mysqli->errno) { printf("Could not insert record into table: %s<br />", $mysqli->error); }
		
		print " -- TABLED CREATED SUCCEFULL -- \n\n";
	}
	
	function tableExists($mysqli,$table)
	{
		$selectQuery = $mysqli->prepare("SHOW TABLES LIKE '".$table."';");
		if(!$selectQuery){ echo "Prepare failed: (". $conn->errno.") ".$conn->error."<br>";}

		$selectQuery->execute();
		$selectQueryResult = $selectQuery->get_result();
		// Loop 
		while($selectQueryRow = $selectQueryResult->fetch_array()) {
			return true;
		}
		
		return false;
	}
	
	// ---
	// -- Funcion que sincroniza cualquier endopoint
	// ---
	function syncEndPoint($token,$mysqli,$endpoint,$primary_key=null,$domain_id)
	{
		global $base_url;	
		$url = $base_url.$endpoint;
				
		$table = $endpoint;
		$table_arr = explode("/",$table);
		
		$table = $table_arr[count($table_arr)-2]."_".$table_arr[count($table_arr)-1];
		
		if(!isset($primary_key)) $primary_key="domain_id";
		
		// --
		// Fist part, request into monsido
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url."?page_size=100000");
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$headers = array(
		   "Accept: application/json",
		   "Authorization: Bearer ".$token."",
		);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$resp = curl_exec($curl);
		curl_close($curl);
		
		$result = json_decode($resp);
		
		// auto-create table
		$table_exists = tableExists($mysqli,$table);
		if($table_exists){ }
		else { buildStructure($mysqli,$table,$primary_key,$result[0]); }
		
		// --
		// Second part, save into database
		foreach($result as $resp)
		{
				$resultx = $mysqli->query("select * from ".$table." where ".$primary_key." = '".$resp->id."'");
				$row = $resultx->fetch_row();

				if(!isset($row) || $row == null)
				{
					// ok insert
					$sql = "INSERT INTO ".$table." (".$primary_key.", domain_id) values ('".trim($resp->id)."','".$domain_id."');";
					mysqli_query($mysqli, $sql);
					
					print $sql;
				}
				else
				{
					// ---
					// OK update contact				
					$update_query = buildUpdate($resp);
					$sql = "UPDATE ".$table." SET ".$update_query." where ".$primary_key." = '".trim($resp->id)."';";
					
					print $sql;
					mysqli_query($mysqli, $sql);
				}
		}

		// ---
		// Return the response as array
		return $result;
	}
		
	function buildUpdate($item_array)
	{
		$update_string="";
		
		foreach($item_array as $field => $value)
		{
			if(!is_array($value) && $field != "id" && $field != "domain_id")
			{
				$update_string .= $field."='".$value."',";
			}
		}
		
		$update_string = substr(trim($update_string),0,-1);
		return $update_string;
	}
	
	
	function getDomains($token,$mysqli)
	{
		global $base_url;	
		$url = $base_url."domains";

		// --
		// Fist part, request into monsido
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$headers = array(
		   "Accept: application/json",
		   "Authorization: Bearer ".$token."",
		);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$resp = curl_exec($curl);
		curl_close($curl);
		
		$result = json_decode($resp);
		
		// --
		// Second part, save into database
		
		foreach($result as $resp){


				$resultx = $mysqli->query("select * from domains where domain_id = '".$resp->id."'");
				$row = $resultx->fetch_row();

				if(!isset($row) || $row == null)
				{
					// ok insert
					$sql = "INSERT INTO domains (domain_id) values ('".$resp->id."');";
					mysqli_query($mysqli, $sql);
				}
				else
				{
					// ---
					// OK update contact				
					$sql = "UPDATE domains SET title='".$resp->title."', url='".$resp->url."', cms='".$resp->cms."', language='".$resp->language."' WHERE domain_id= '".$resp->id."';";
					mysqli_query($mysqli, $sql);
				}
		}

		// ---
		// Return the response as array
		return $result;
	}
	
	// -- Getting SEO SUMMARY
	// --
	function getSeoSummary($token,$mysqli,$domain_id)
	{
		global $base_url;	
		$url = $base_url."/domains/".$domain_id."/seo/summary";
		
		// --
		// Fist part, request into monsido
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$headers = array(
		   "Accept: application/json",
		   "Authorization: Bearer ".$token."",
		);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$resp = curl_exec($curl);
		curl_close($curl);
		
		$result = json_decode($resp);
		
		$resp = $result;


		$resultx = $mysqli->query("select * from seo_summary where domain_id = '".$domain_id."'");
		$row = $resultx->fetch_row();

		if(!isset($row) || $row == null)
		{
			// ok insert
			$sql = "INSERT INTO seo_summary (domain_id) values ('".$domain_id."');";
			mysqli_query($mysqli, $sql);
		}
		else
		{
			// ---
			// OK update contact				
			$sql = "UPDATE seo_summary SET pages_with_seo_errors='".$resp->pages_with_seo_errors."', seo_errors='".$resp->seo_errors."', pages_count='".$resp->pages_count."', compliance_percentage='".$resp->compliance_percentage."', seo_errors_alerts_count='".$resp->seo_errors_alerts_count."', seo_errors_warnings_count='".$resp->seo_errors_warnings_count."', seo_errors_infos_count='".$resp->seo_errors_infos_count."', seo_errors_technicals_count='".$resp->seo_errors_technicals_count."' WHERE domain_id= '".$domain_id."';";
			mysqli_query($mysqli, $sql);
		}

		// ---
		// Return the response as array
		return $result;
	}
	
	
	// -- Getting Quality SUMMARY
	// --
	function getQualitySummary($token,$mysqli,$domain_id)
	{
		global $base_url;	
		$url = $base_url."domains/".$domain_id."/quality/summary";

		// --
		// Fist part, request into monsido
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$headers = array(
		   "Accept: application/json",
		   "Authorization: Bearer ".$token."",
		);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$resp = curl_exec($curl);
		curl_close($curl);
		
		$result = json_decode($resp);
		$resp = $result;

		$resultx = $mysqli->query("select * from quality_summary where domain_id = '".$domain_id."'");
		$row = $resultx->fetch_row();

		if(!isset($row) || $row == null)
		{
			// ok insert
			$sql = "INSERT INTO quality_summary (domain_id) values ('".$domain_id."');";
			mysqli_query($mysqli, $sql);
		}
		else
		{
			// ---
			// OK update contact				
			$sql = "UPDATE quality_summary SET broken_links='".$resp->broken_links."', pages_with_broken_links='".$resp->pages_with_broken_links."', broken_images='".$resp->broken_images."', pages_with_broken_images='".$resp->pages_with_broken_images."', confirmed_misspellings='".$resp->confirmed_misspellings."', potential_misspellings='".$resp->potential_misspellings."', potential_ni_numbers='".$resp->potential_ni_numbers."', pages_with_confirmed_misspellings='".$resp->pages_with_confirmed_misspellings."', pages_with_potential_misspellings='".$resp->pages_with_potential_misspellings."', pages_with_quality_notifications='".$resp->pages_with_quality_notifications."', pages_without_quality_notifications='".$resp->pages_without_quality_notifications."', quality_notifications='".$resp->quality_notifications."', documents_with_broken_links='".$resp->documents_with_broken_links."', documents_with_broken_images='".$resp->documents_with_broken_images."', documents_with_confirmed_misspellings='".$resp->documents_with_confirmed_misspellings."', documents_with_potential_misspellings='".$resp->documents_with_potential_misspellings."', documents_with_quality_notifications='".$resp->documents_with_quality_notifications."', documents_without_quality_notifications='".$resp->documents_without_quality_notifications."', pages_with_quality_notifications_not_searches='".$resp->pages_with_quality_notifications_not_searches."', documents_with_quality_notifications_not_searches='".$resp->documents_with_quality_notifications_not_searches."'
			
			WHERE domain_id= '".$domain_id."';";
			mysqli_query($mysqli, $sql);
		}

		// ---
		// Return the response as array
		return $result;
	}

	// --	
	// - Getting SEO PAGES.
	function getSeoPages($token,$mysqli,$domain_id)
	{
		global $base_url;	
		$url = $base_url."domains/".$domain_id."/seo/pages";
		
		// --
		// Fist part, request into monsido
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$headers = array(
		   "Accept: application/json",
		   "Authorization: Bearer ".$token."",
		);
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		
		//for debug only!
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$resp = curl_exec($curl);
		curl_close($curl);
		
		$result = json_decode($resp);
		
		foreach($result as $resp){

			$resultx = $mysqli->query("select * from seo_pages where page_id = '".$resp->id."'");
			$row = $resultx->fetch_row();
	
			if(!isset($row) || $row == null)
			{
				// ok insert
				$sql = "INSERT INTO seo_pages (page_id) values ('".$resp->id."');";
				mysqli_query($mysqli, $sql);
			}
			else
			{
				// ---
				// OK update contact				
				$sql = "UPDATE seo_pages SET title='".$resp->title."', url='".$resp->url."', mrank='".$resp->mrank."', hit_score='".$resp->hit_score."', link_score='".$resp->link_score."', error_score='".$resp->error_score."', priority_score='".$resp->priority_score."', detected_language='".$resp->detected_language."', language_override='".$resp->language_override."', no_index='".$resp->no_index."', hits='".$resp->hits."', type='".$resp->type."', dead_links_count='".$resp->dead_links_count."', ignored_links_count='".$resp->ignored_links_count."', fixed_links_count='".$resp->fixed_links_count."', dead_images_count='".$resp->dead_images_count."', ignored_images_count='".$resp->ignored_images_count."', fixed_images_count='".$resp->fixed_images_count."', seo_issues_count='".$resp->seo_issues_count."', spelling_errors_count='".$resp->spelling_errors_count."', notification_count='".$resp->notification_count."', spelling_errors_confirmed_count='".$resp->spelling_errors_confirmed_count."', spelling_errors_potential_count='".$resp->spelling_errors_potential_count."', accessibility_checks_with_errors_count='".$resp->accessibility_checks_with_errors_count."', accessibility_ignored_checks_count='".$resp->accessibility_ignored_checks_count."', nin_count='".$resp->nin_count."', quality_notification_count='".$resp->quality_notification_count."', cms_url='".$resp->cms_url."', words_count='".$resp->words_count."', readability_words_count='".$resp->readability_words_count."', language='".$resp->language."', priority_score_as_text='".$resp->priority_score_as_text."', readability='".$resp->readability."', readability_level='".$resp->readability_level."', policy_violation_count='".$resp->policy_violation_count."', policy_search_count='".$resp->policy_search_count."', policy_required_count='".$resp->policy_required_count."', lastcheck='".$resp->lastcheck."', latest_page_scan_status='".$resp->latest_page_scan_status."', created_at='".$resp->created_at."', updated_at='".$resp->updated_at."', data_protection_violations_counts='".$resp->data_protection_violations_counts."', error_searches_count='".$resp->error_searches_count."', searches_count='".$resp->searches_count."', quality_notifications_not_searches='".$resp->quality_notifications_not_searches."'			
				WHERE page_id= '".$resp->id."';";
				
				mysqli_query($mysqli, $sql);
			}
		}

		// ---
		// Return the response as array
		return $result;
	}

	
?>