<?php
	
	ini_set('memory_limit','2048M');
	
	// mysql connections
	$mysqli = new mysqli("localhost", "root", "linux666","oster_mailing");
	
	
	
	
	// --	
	// Primero importo los CSV
	// --

	/* FILE GR_ANALYTICS*/
	// --
	// FIX CSV
	$csv1 = file_get_contents(dirname(__FILE__)."/csv/gr_analytics.csv");	
	if(substr_count($csv1, ';') > 0)
	{
		$csv1 = str_replace(",","",$csv1);
		$csv1 = str_replace(";",",",$csv1);	
		file_put_contents(dirname(__FILE__)."/csv/gr_analytics.csv",$csv1);		
	}
	 
	// -------
	// -- gr_analytics (Todos los canales, filtrando por email, source medium)
	$gr_analytics_file = dirname(__FILE__)."/csv/gr_analytics.csv";
    $csvFile = fopen($gr_analytics_file, 'r');
     
    // truncate gr_analytics
	$sql = "TRUNCATE gr_analytics;"; mysqli_query($mysqli, $sql);
     
    // Parse data from CSV file line by line
    while(($line = fgetcsv($csvFile)) !== FALSE){
    	
			// DEBUG print_r($line); exit;
			$line[9] = str_ireplace("CLP ","",$line[9]);
			$sql = "INSERT INTO gr_analytics (utm,users,new_users,sessions,bounce_rate,pages_session,avg_session,ecommerce_conversion_rate,transactions,revenue) VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."','".$line[5]."','".$line[6]."','".$line[7]."','".($line[8]/100)."','".$line[9]."');";
			mysqli_query($mysqli, $sql);
    }   
	 
	/* FILE gr_analytics_products*/
	// -- gr_analytics_products (Conversions - Ecommerce - Product performance - Primary product)
	$csv1 = file_get_contents(dirname(__FILE__)."/csv/gr_analytics_category_products.csv");	
	if(substr_count($csv1, ';') > 0)
	{
		$csv1 = str_replace(",","",$csv1);
		$csv1 = str_replace(";",",",$csv1);	
		file_put_contents(dirname(__FILE__)."/csv/gr_analytics_category_products.csv",$csv1);		
	}
	 
	// -------
	// -- gr_analytics (Todos los canales, filtrando por email, source medium)
	$gr_analytics_file = dirname(__FILE__)."/csv/gr_analytics_category_products.csv";
    $csvFile = fopen($gr_analytics_file, 'r');
     
    // truncate gr_analytics
	$sql = "TRUNCATE gr_analytics_category_products;"; mysqli_query($mysqli, $sql);
     
    // Parse data from CSV file line by line
    while(($line = fgetcsv($csvFile)) !== FALSE){
    	
			// DEBUG  print_r($line); exit;
			$line[2] = str_ireplace("CLP ","",$line[2]);
			$line[2] = str_ireplace(",","",$line[2]);
			$line[2] = str_ireplace('"',"",$line[2]);
			
			$line[5] = str_ireplace("CLP ","",$line[5]);
			$line[5] = str_ireplace(",","",$line[5]);
			$line[5] = str_ireplace('"',"",$line[5]);
			
			$sql = "INSERT INTO gr_analytics_category_products (product,utm_source_medium,revenue,unique_purchases,quantity,avg_price) VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."','".$line[5]."');";
			mysqli_query($mysqli, $sql);
    }
	
	/* FILE gr_analytics_categories*/
	// -- gr_analytics_categories-products (Conversions - Ecommerce - Product performance - Primary category)
	$csv1 = file_get_contents(dirname(__FILE__)."/csv/gr_analytics_products.csv");	
	if(substr_count($csv1, ';') > 0)
	{
		$csv1 = str_replace(",","",$csv1);
		$csv1 = str_replace(";",",",$csv1);	
		file_put_contents(dirname(__FILE__)."/csv/gr_analytics_products.csv",$csv1);		
	}
	 
	// -------
	// -- gr_analytics (Todos los canales, filtrando por email, source medium)
	$gr_analytics_file = dirname(__FILE__)."/csv/gr_analytics_products.csv";
    $csvFile = fopen($gr_analytics_file, 'r');
     
    // truncate gr_analytics
	$sql = "TRUNCATE gr_analytics_products;"; mysqli_query($mysqli, $sql);
     
    // Parse data from CSV file line by line
    while(($line = fgetcsv($csvFile)) !== FALSE){
    	
			// DEBUG  print_r($line); exit;
			$line[2] = str_ireplace("CLP ","",$line[2]);
			$line[2] = str_ireplace(",","",$line[2]);
			$line[2] = str_ireplace('"',"",$line[2]);
			
			$line[5] = str_ireplace("CLP ","",$line[5]);
			$line[5] = str_ireplace(",","",$line[5]);
			$line[5] = str_ireplace('"',"",$line[5]);
			
			$sql = "INSERT INTO gr_analytics_products (product,utm_source_medium,revenue,unique_purchases,quantity,avg_price) VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','".$line[3]."','".$line[4]."','".$line[5]."');";
			mysqli_query($mysqli, $sql);
    }
	

	
	// ---
	// OK Cargo gr_newsletter
	$sql = "TRUNCATE gr_analytics_category_products_newsletters;"; mysqli_query($mysqli, $sql);
	
	$result = $mysqli->query("SELECT * FROM gr_newsletters where country = 'Chile'");
	if($result->num_rows){
	    while ($row = $result->fetch_all(MYSQLI_ASSOC)) {
	        foreach ($row as $r){
		        
		        // slugify
		        $name = $r["name"];
		        $name = str_ireplace(" ","-",$name);
		        $name = substr($name,0,14);
		        
		        $name2 = $r["name"];
		        $name2 = str_ireplace(" ","",$name2);
		        $name2 = substr($name2,0,14);
		        
		        // OK busco en los UTM
		        $users =0;
		        $new_users=0;
		        $sessions=0;
		        $bounce_rate=0;
		        $pages_session=0;
		        $avg_session=0;
		        $ecommerce_conversion_rate=0;
		        $transactions=0;
		        $revenue=0;
		        
		        // NEWSLETTERS 
				$result_clasif = $mysqli->query("SELECT * FROM gr_analytics where utm like '%".$name."%'");
				if($result_clasif->num_rows){
				    while ($row_clasif = $result_clasif->fetch_all(MYSQLI_ASSOC)) {
						foreach ($row_clasif as $rc){
							
					        $users =$users+$rc["users"];
					        $new_users=$new_users+$rc["new_users"];
					        $sessions=$sessions+$rc["sessions"];
					        $bounce_rate=$rc["bounce_rate"];
					        $pages_session=$rc["pages_session"];
					        $avg_session=$rc["avg_session"];
					        $ecommerce_conversion_rate=$rc["ecommerce_conversion_rate"];
					        $transactions=$transactions+$rc["transactions"];
					        $revenue=$revenue+$rc["revenue"];
						}
					}
					
					// ---
					// OK UPDATE
					$sql = "UPDATE gr_newsletters set analytics_users='".$users."', analytics_new_users='".$new_users."', analytics_sessions='".$sessions."', analytics_bounce_rate='".$bounce_rate."', analytics_pages_per_sessions='".$pages_session."', analytics_avg_sessions='".$avg_session."', analytics_ecommerce_conversion_rate='".$ecommerce_conversion_rate."', analytics_transactions='".$transactions."',analytics_revenue='".$revenue."' where newsletterId = '".$r["newsletterId"]."'";
					mysqli_query($mysqli, $sql);
				}
				
 
				// ---
				// Busco POR CATEGORIAS
		        $name = $r["name"];
		        $name = str_ireplace(" ","-",$name);
		        $name = substr($name,0,14);
		        
		        $name2 = $r["name"];
		        $name2 = str_ireplace(" ","",$name2);
		        $name2 = substr($name2,0,14);
		        
				$result_clasif = $mysqli->query("SELECT * FROM gr_analytics_category_products where utm_source_medium like '%".$name."%'");
				if($result_clasif->num_rows){
					
			        $quantity=0;
			        $unique_purchases=0;
			        $revenue=0;
					
					
				    while ($row_clasif = $result_clasif->fetch_all(MYSQLI_ASSOC)) {
						foreach ($row_clasif as $rc){
							
					        $revenue =$revenue+$rc["revenue"];
					        $unique_purchases=$unique_purchases+$rc["unique_purchases"];
					        $quantity=$quantity+$rc["quantity"];
						}
					}
					
					// ---
					// OK UPDATE
					$sql = "INSERT INTO gr_analytics_category_products_newsletters (newsletterId,product,utm_source_medium,revenue,unique_purchases,quantity) VALUES ('".$r["newsletterId"]."','".$rc["product"]."','".$rc["utm_source_medium"]."','".$revenue."','".$unique_purchases."','".$quantity."')";
					mysqli_query($mysqli, $sql);
				}
				
				
				// ---
				// Busco POR PRODUCTOS
		        $name = $r["name"];
		        $name = str_ireplace(" ","-",$name);
		        $name = substr($name,0,14);
		        
		        $name2 = $r["name"];
		        $name2 = str_ireplace(" ","",$name2);
		        $name2 = substr($name2,0,14);
		        
				$result_clasif = $mysqli->query("SELECT * FROM gr_analytics_products where utm_source_medium like '%".$name."%'");
				if($result_clasif->num_rows){
					
			        $quantity=0;
			        $unique_purchases=0;
			        $revenue=0;
					
					
				    while ($row_clasif = $result_clasif->fetch_all(MYSQLI_ASSOC)) {
						foreach ($row_clasif as $rc){
							
					        $revenue =$revenue+$rc["revenue"];
					        $unique_purchases=$unique_purchases+$rc["unique_purchases"];
					        $quantity=$quantity+$rc["quantity"];
						}
					}
					
					// ---
					// OK UPDATE
					$sql = "INSERT INTO gr_analytics_products_newsletters (newsletterId,product,utm_source_medium,revenue,unique_purchases,quantity) VALUES ('".$r["newsletterId"]."','".$rc["product"]."','".$rc["utm_source_medium"]."','".$revenue."','".$unique_purchases."','".$quantity."')";
					mysqli_query($mysqli, $sql);
				}
				
				print $r["newsletterId"]."\n";				
		     }
		}
	}
	
	
	
	
	
	// gr_analytics_category_products_newsletters 
	
	
	
	
	
?>